# [gksu](https://gitlab.com/XRevan86/gksu), a GTK+ front end for /bin/su

gksu supports login shells and preserving environment when acting as a su or
sudo front end. It is useful for menu items or other graphical applications
that need to ask the user's password to run as another user.

This project has been initiated by Gustavo Noronha Silva in February of 2002.

Reports are very welcome at [GitLab Issues](https://gitlab.com/XRevan86/gksu/issues).
