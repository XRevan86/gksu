/*
 * gksu -- GTK+ Frontend to su
 * Copyright (C) 2002-2009 Gustavo Noronha Silva <kov@debian.org>
 * Copyright (C) 2016 Alexei Sorokin <sor.alexei@meowr.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see
 * <https://gnu.org/licenses>.
 */

#ifndef __DEFINES_H__
#define __DEFINES_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <locale.h>

#endif
