# Hebrew translation for gksu
# Copyright (C) 2006 Rosetta Contributors and Canonical Ltd 2006
# This file is distributed under the same licence as the gksu package.
#
msgid ""
msgstr ""
"Project-Id-Version: gksu\n"
"Report-Msgid-Bugs-To: kov@debian.org\n"
"POT-Creation-Date: 2017-05-04 16:51+0300\n"
"PO-Revision-Date: 2010-09-03 23:30+0000\n"
"Last-Translator: Yaron <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gksu/gksu.c:90
#, c-format
msgid ""
"GKsu version %s\n"
"\n"
msgstr ""
"GKsu version %s\n"
"\n"

#: ../gksu/gksu.c:91
#, c-format
msgid ""
"Usage: %s [-u <user>] [options] <command>\n"
"\n"
msgstr ""
"Usage: %s [-u <user>] [options] <command>\n"
"\n"

#: ../gksu/gksu.c:92
msgid ""
"  --debug, -d\n"
"    Print information on the screen that might be\n"
"    useful for diagnosing and/or solving problems.\n"
msgstr ""
"  --debug, -d\n"
"    Print information on the screen that might be\n"
"    useful for diagnosing and/or solving problems.\n"

#: ../gksu/gksu.c:95 ../gksu/gksu.c:98 ../gksu/gksu.c:114 ../gksu/gksu.c:125
#: ../gksu/gksu.c:131
msgid "\n"
msgstr "\n"

#: ../gksu/gksu.c:96
msgid ""
"  --user <user>, -u <user>\n"
"    Call <command> as the specified user.\n"
msgstr ""
"  --user <user>, -u <user>\n"
"    Call <command> as the specified user.\n"

#: ../gksu/gksu.c:99
msgid ""
"  --disable-grab, -g\n"
"    Disable the \"locking\" of the keyboard, mouse,\n"
"    and focus done by the program when asking for\n"
"    password.\n"
msgstr ""
"  --disable-grab, -g\n"
"    Disable the \"locking\" of the keyboard, mouse,\n"
"    and focus done by the program when asking for\n"
"    password.\n"

#: ../gksu/gksu.c:103
msgid ""
"  --prompt, -P\n"
"    Ask the user if they want to have their keyboard\n"
"    and mouse grabbed before doing so.\n"
msgstr ""
"  --prompt, -P\n"
"    Ask the user if they want to have their keyboard\n"
"    and mouse grabbed before doing so.\n"

#: ../gksu/gksu.c:106
msgid ""
"  --preserve-env, -k\n"
"    Preserve the current environments, does not set $HOME\n"
"    nor $PATH, for example.\n"
msgstr ""
"  --preserve-env, -k\n"
"    Preserve the current environments, does not set $HOME\n"
"    nor $PATH, for example.\n"

#: ../gksu/gksu.c:109
msgid ""
"  --login, -l\n"
"    Make this a login shell. Beware this may cause\n"
"    problems with the Xauthority magic. Run xhost\n"
"    to allow the target user to open windows on your\n"
"    display!\n"
msgstr ""
"  --login, -l\n"
"    Make this a login shell. Beware this may cause\n"
"    problems with the Xauthority magic. Run xhost\n"
"    to allow the target user to open windows on your\n"
"    display!\n"

#: ../gksu/gksu.c:115
msgid ""
"  --description <description|file>, -D <description|file>\n"
"    Provide a descriptive name for the command to\n"
"    be used in the default message, making it nicer.\n"
"    You can also provide the absolute path for a\n"
"    .desktop file. The Name key for will be used in\n"
"    this case.\n"
msgstr ""
"  --description <description|file>, -D <description|file>\n"
"    Provide a descriptive name for the command to\n"
"    be used in the default message, making it nicer.\n"
"    You can also provide the absolute path for a\n"
"    .desktop file. The Name key for will be used in\n"
"    this case.\n"

#: ../gksu/gksu.c:121
msgid ""
"  --message <message>, -m <message>\n"
"    Replace the standard message shown to ask for\n"
"    password for the argument passed to the option.\n"
"    Only use this if --description does not suffice.\n"
msgstr ""
"  --message <message>, -m <message>\n"
"    Replace the standard message shown to ask for\n"
"    password for the argument passed to the option.\n"
"    Only use this if --description does not suffice.\n"

#: ../gksu/gksu.c:126
msgid ""
"  --print-pass, -p\n"
"    Ask gksu to print the password to stdout, just\n"
"    like ssh-askpass. Useful to use in scripts with\n"
"    programs that accept receiving the password on\n"
"    stdin.\n"
msgstr ""
"  --print-pass, -p\n"
"    Ask gksu to print the password to stdout, just\n"
"    like ssh-askpass. Useful to use in scripts with\n"
"    programs that accept receiving the password on\n"
"    stdin.\n"

#: ../gksu/gksu.c:132
msgid ""
"  --sudo-mode, -S\n"
"    Make GKSu use sudo instead of su, as if it had been\n"
"    run as \"gksudo\".\n"
msgstr ""
"  --sudo-mode, -S\n"
"    גרום ל GKSU להשתמש ב SU במקום SUDO, כאילו\n"
"    השתמשו ב GKSUDO\n"

#: ../gksu/gksu.c:135
msgid ""
"  --su-mode, -w\n"
"    Make GKSu use su, instead of using libgksu's\n"
"    default.\n"
msgstr ""
"  --su-mode, -w\n"
"    Make GKSu use su, instead of using libgksu's\n"
"    default.\n"

#: ../gksu/gksu.c:244
msgid "Advanced options"
msgstr "אפשרויות מתקדמות"

#: ../gksu/gksu.c:249
msgid "_Close"
msgstr "_סגירה"

#: ../gksu/gksu.c:261
msgid "<b>Options to use when changing user</b>"
msgstr "<b>אפשרויות לשימוש בעת החלפת משתמש</b>"

#. login shell? (--login)
#: ../gksu/gksu.c:266
msgid "_login shell"
msgstr "_מעטפת כניסה"

#: ../gksu/gksu.c:274
msgid "_preserve environment"
msgstr "_שימור סביבה"

#: ../gksu/gksu.c:382
msgid "Run program"
msgstr "הרצת תכנית"

#: ../gksu/gksu.c:385
msgid "_Cancel"
msgstr "_ביטול"

#: ../gksu/gksu.c:390
msgid "_OK"
msgstr "_אישור"

#. command
#: ../gksu/gksu.c:416
msgid "Run:"
msgstr "הרצה:"

#. user name
#: ../gksu/gksu.c:427
msgid "As user:"
msgstr "בתור המשתמש:"

#. advanced button
#: ../gksu/gksu.c:453
msgid "_Advanced"
msgstr "מ_תקדם"

#: ../gksu/gksu.c:493 ../gksu/gksu.c:715
msgid "Missing command to run."
msgstr "חסרה פקודה להרצה."

#: ../gksu/gksu.c:597
#, c-format
msgid "Option not accepted for --disable-grab: %s\n"
msgstr "האפשרות אינה מקובלת על ידי ‎--disable-grab:‏ %s\n"

#: ../gksu/gksu.c:629
#, c-format
msgid "Option not accepted for --prompt: %s\n"
msgstr "האפשרות אינה מקובלת על ידי ‎--prompt:‏ %s\n"

#: ../gksu/gksu.c:655
msgid ""
"<b>Would you like your screen to be \"grabbed\"\n"
"while you enter the password?</b>\n"
"\n"
"This means all applications will be paused to avoid\n"
"the eavesdropping of your password by a malicious\n"
"application while you type it."
msgstr ""
"<b>האם ברצונך שהמסך \"ילכד\"\n"
"בזמן הקלדת הססמה?</b>\n"
"\n"
"משמעות הדבר היא שכל התוכנות\n"
"יושהו כדי למנוע ציתות לססמתך\n"
"על־ידי יישום זדוני בזמן הקלדתה."

#: ../gksu/gksu.c:679
msgid ""
"<big><b>Missing options or arguments</b></big>\n"
"\n"
"You need to provide --description or --message."
msgstr ""
"<big><b>חסרות אפשרויות או ארגומנטים</b></big>\n"
"\n"
"יש לספק  ‎--description או ‎--message בשורת הפקודה."

#: ../gksu/gksu.c:689
#, c-format
msgid ""
"<b>Failed to request password.</b>\n"
"\n"
"%s"
msgstr ""
"<b>בקשת הססמה נכשלה</b>\n"
"\n"
"%s"

#: ../gksu/gksu.c:763
#, c-format
msgid "User %s does not exist."
msgstr "המשתמש %s אינו קיים."

#: ../gksu/gksu.c:783
msgid "<b>Incorrect password... try again.</b>"
msgstr "<b>ססמה שגויה... נא לנסות שוב.</b>"

#: ../gksu/gksu.c:821
#, c-format
msgid ""
"<b>Failed to run %s as user %s.</b>\n"
"\n"
"%s"
msgstr ""
"<b>אירע כשל בהרצת %s בתור המשתמש %s.</b>\n"
"\n"
"%s"

#: ../nautilus-gksu/libnautilus-gksu.c:137
msgid "Open as administrator"
msgstr "פתיחה כמנהל מערכת"

#: ../nautilus-gksu/libnautilus-gksu.c:138
msgid "Opens the file with administrator privileges"
msgstr "פתיחת קובץ עם הרשאות מנהל מערכת"

#: ../nautilus-gksu/libnautilus-gksu.c:226
msgid ""
"<big><b>Unable to determine the program to run.</b></big>\n"
"\n"
"The item you selected cannot be opened with administrator privileges because "
"the correct application cannot be determined."
msgstr ""
"<big><b>זיהוי התכנית להפעלה נכשל.</b></big>\n"
"\n"
"לא ניתן לפתוח את הפריט שבחרת עם הרשאת מנהל מערכת כי הניסיון למציאת היישום "
"המתאים להרצה נכשל."

#~ msgid "Root Terminal"
#~ msgstr "מסוף משתמש על"

#~ msgid ""
#~ "Opens a terminal as the root user, using gksu to ask for the password"
#~ msgstr "נפתח מסוף בתור משתמש על (root) תוך שימוש ב־gksu לקבלת הססמה."
